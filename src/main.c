/**
 * Copyright (c) 2014-2018 Microchip Technology Inc. and its subsidiaries.
 * Copyright (c) 2020-2023 Linaro Limited
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 */

#include <asf.h>
#include <component/sercom.h>

uint8_t usb_serial_number[USB_DEVICE_GET_SERIAL_NAME_LENGTH];

#define PIN_GPIO_DC_EN PIN_PA16
#define PIN_GPIO_VBUS_EN PIN_PA02
#define PIN_TXS_EN	PIN_PA07

#define F_CPU	8000000

#define UART_FIFO_SIZE 64

static volatile uint8_t uart_buf[2][UART_FIFO_SIZE];
static volatile uint8_t dmac_uart_buf;

static void uart_sync_bus(Sercom *sercom)
{
	while (sercom->USART.SYNCBUSY.reg)
	;
}

static void qcomlt_debug_uart_init(unsigned int baud)
{
	// TOFIX baudrate is fixed to 115200
	uint64_t br = 63019; //(uint64_t)65536 * (F_CPU - 16 * baud) / F_CPU;

	PM->APBCMASK.reg |= PM_APBCMASK_SERCOM0;

	GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SERCOM0_GCLK_ID_CORE) |
			    GCLK_CLKCTRL_CLKEN |
			    GCLK_CLKCTRL_GEN(0);

	SERCOM0->USART.CTRLA.reg = SERCOM_USART_CTRLA_DORD |
				   SERCOM_USART_CTRLA_FORM(4) |
				   SERCOM_USART_CTRLA_MODE_USART_INT_CLK |
				   SERCOM_USART_CTRLA_RXPO(1) |
				   SERCOM_USART_CTRLA_TXPO(0) |
				   SERCOM_USART_CTRLA_RUNSTDBY;
	uart_sync_bus(SERCOM0);

	SERCOM0->USART.CTRLB.reg = SERCOM_USART_CTRLB_RXEN |
				   SERCOM_USART_CTRLB_TXEN |
				   SERCOM_USART_CTRLB_CHSIZE(0) |
				   SERCOM_USART_CTRLB_SFDE;
	uart_sync_bus(SERCOM0);

	SERCOM0->USART.BAUD.reg = (uint16_t)br + 1;
	uart_sync_bus(SERCOM0);
	SERCOM0->USART.CTRLA.reg |= SERCOM_USART_CTRLA_ENABLE;
	uart_sync_bus(SERCOM0);
}

static void qcomlt_debug_uart_putc(int ch)
{
	while (!(SERCOM0->USART.INTFLAG.reg & SERCOM_USART_INTFLAG_DRE))
		;

	SERCOM0->USART.DATA.reg = ch;
}

static volatile DmacDescriptor dmac_wrb[12] __attribute__ ((aligned (16)));
static DmacDescriptor dmac_descs[12] __attribute__ ((aligned (16)));

static void dmac_uart_start(void)
{
	PM->APBAMASK.reg |= PM_APBBMASK_DMAC;

	DMAC->CTRL.reg = 0;
	DMAC->BASEADDR.reg = (uint32_t)dmac_descs;
	DMAC->WRBADDR.reg = (uint32_t)dmac_wrb;
	DMAC->CTRL.reg = DMAC_CTRL_DMAENABLE | DMAC_CTRL_LVLEN(0xf);

	DMAC->CHID.reg = DMAC_CHID_ID(0);
	DMAC->CHCTRLA.reg = DMAC_CHCTRLA_SWRST;
	while (DMAC->CHCTRLA.bit.SWRST)
		;
	DMAC->CHCTRLB.reg = DMAC_CHCTRLB_LVL(0) |
			    DMAC_CHCTRLB_TRIGSRC(SERCOM0_DMAC_ID_RX) |
			    DMAC_CHCTRLB_TRIGACT_BEAT;
	/* No interrupts for this channel, rely on SOF to ping-pong the uart buf */
	DMAC->CHINTENSET.reg = 0;

	dmac_descs[0].DESCADDR.reg = 0;
	dmac_descs[0].SRCADDR.reg = (uint32_t)&SERCOM0->USART.DATA.reg;
	/*
	 * https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Datasheet.pdf page 283 - 19.6.2.7 Addressing
	 * When destination address incrementation is configured (BTCTRL.DSTINC is one),
	 * DSTADDR must be set to the destination address of the last beat transfer in the block transfer
	 */
	dmac_descs[0].DSTADDR.reg = (uint32_t)(uart_buf[dmac_uart_buf] + 64);
	dmac_descs[0].BTCNT.reg = 64;
	dmac_descs[0].BTCTRL.reg = DMAC_BTCTRL_VALID |
				   DMAC_BTCTRL_BEATSIZE_BYTE |
				   DMAC_BTCTRL_STEPSEL |
				   DMAC_BTCTRL_DSTINC;

	DMAC->CHINTFLAG.reg = DMAC->CHINTFLAG.reg;
	DMAC->CHCTRLA.reg = DMAC_CHCTRLA_ENABLE;
}

static void dmac_sof_tick(void)
{
	uint8_t cur_buf = dmac_uart_buf;
	uint8_t avail;
	uint8_t len;

	/* Ensure that there's space in the CDC buffer, or it will hang */
	avail = udi_cdc_multi_get_free_tx_buffer(1);

	DMAC->CHID.reg = DMAC_CHID_ID(0);
	/* Stop the channel to ensure that the write-back descriptor is updated */
	DMAC->CHCTRLA.reg = 0;
	while (DMAC->CHCTRLA.reg & DMAC_CHCTRLA_ENABLE)
		;
	len = dmac_descs[0].BTCNT.reg - dmac_wrb[0].BTCNT.reg;
	if (len == 0 || avail < len) {
		DMAC->CHCTRLA.reg = DMAC_CHCTRLA_ENABLE;
		return;
	}

	dmac_uart_buf = !dmac_uart_buf;
	/*
	 * https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Datasheet.pdf page 283 - 19.6.2.7 Addressing
	 * When destination address incrementation is configured (BTCTRL.DSTINC is one),
	 * DSTADDR must be set to the destination address of the last beat transfer in the block transfer
	 */
	dmac_descs[0].DSTADDR.reg = (uint32_t)(uart_buf[dmac_uart_buf] + 64);
	DMAC->CHCTRLA.reg = DMAC_CHCTRLA_ENABLE;

	udi_cdc_multi_write_buf(1, (const void *)uart_buf[cur_buf], len);
}

static void i2c_sync_bus(Sercom *sercom)
{
	while (sercom->I2CM.SYNCBUSY.reg)
		;
}

static void i2c_init(void)
{
	PM->APBCMASK.reg |= PM_APBCMASK_SERCOM1;

	GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SERCOM1_GCLK_ID_CORE) |
			    GCLK_CLKCTRL_CLKEN |
			    GCLK_CLKCTRL_GEN(0);

	SERCOM1->I2CM.CTRLB.reg = SERCOM_I2CM_CTRLB_SMEN;
	i2c_sync_bus(SERCOM1);

	SERCOM1->I2CM.BAUD.reg = SERCOM_I2CM_BAUD_BAUD(48);
	i2c_sync_bus(SERCOM1);

	SERCOM1->I2CM.CTRLA.reg = SERCOM_I2CM_CTRLA_ENABLE |
				  SERCOM_I2CM_CTRLA_MODE_I2C_MASTER |
				  SERCOM_I2CM_CTRLA_SDAHOLD(3);
	i2c_sync_bus(SERCOM1);

	SERCOM1->I2CM.STATUS.reg |= SERCOM_I2CM_STATUS_BUSSTATE(1);
	i2c_sync_bus(SERCOM1);
}

static unsigned int i2c_wait_int_mb(Sercom *sercom)
{
	while (!(sercom->I2CM.INTFLAG.reg & SERCOM_I2CM_INTFLAG_MB))
		;

	return sercom->I2CM.STATUS.reg;
}

static unsigned int i2c_wait_int_sb(Sercom *sercom)
{
	while (!(sercom->I2CM.INTFLAG.reg & SERCOM_I2CM_INTFLAG_SB))
		;

	return sercom->I2CM.STATUS.reg;
}

static int pmbus_setup(unsigned int addr, unsigned char cmd, unsigned char is_read)
{
	unsigned int status;

	SERCOM1->I2CM.ADDR.reg = addr;
	status = i2c_wait_int_mb(SERCOM1);
	if (status & SERCOM_I2CM_STATUS_RXNACK)
		return -1;

	SERCOM1->I2CM.DATA.reg = cmd;
	status = i2c_wait_int_mb(SERCOM1);
	if (status & SERCOM_I2CM_STATUS_RXNACK)
		return -1;

	if (is_read) {
		SERCOM1->I2CM.ADDR.reg = addr | 1;
		status =  i2c_wait_int_sb(SERCOM1);
		if (status & SERCOM_I2CM_STATUS_RXNACK)
			return -1;
	}
	return 0;
}

static ssize_t pmbus_read(uint8_t addr, unsigned char cmd, void *buf, size_t len)
{
	uint8_t *p = buf;
	size_t i;
	int err;

	err = pmbus_setup(addr, cmd, 1);
	if (err) {
		SERCOM1->I2CM.CTRLB.reg |= SERCOM_I2CM_CTRLB_CMD(3);
		return -1;
	}

	SERCOM1->I2CM.CTRLB.reg &= ~SERCOM_I2CM_CTRLB_ACKACT;
	for (i = 0; i < len - 1; i++) {
		*p++ = SERCOM1->I2CM.DATA.reg;
		i2c_wait_int_sb(SERCOM1);
	}
	SERCOM1->I2CM.CTRLB.reg |= SERCOM_I2CM_CTRLB_ACKACT;
	SERCOM1->I2CM.CTRLB.reg |= SERCOM_I2CM_CTRLB_CMD(3);
	*p++ = SERCOM1->I2CM.DATA.reg;

	return len;
}

static ssize_t pmbus_write(uint8_t addr, unsigned char cmd, const void *buf, size_t len)
{
	unsigned int status;
	const uint8_t *p = (uint8_t *)buf;
	size_t i;
	int err;
	ssize_t ret = len;

	err = pmbus_setup(addr, cmd, 0);
	if (err) {
		SERCOM1->I2CM.CTRLB.reg |= SERCOM_I2CM_CTRLB_CMD(3);
		return -1;
	}

	SERCOM1->I2CM.CTRLB.reg &= ~SERCOM_I2CM_CTRLB_ACKACT;
	for (i = 0; i < len; i++) {
		SERCOM1->I2CM.DATA.reg = *p++;
		status = i2c_wait_int_mb(SERCOM1);
		if (status & SERCOM_I2CM_STATUS_RXNACK) {
			ret = -2;
			break;
		}
	}
	SERCOM1->I2CM.CTRLB.reg |= SERCOM_I2CM_CTRLB_CMD(3);

	return ret;
}

#define INA233_ADDR 0x80

static void ina233_calibrate(unsigned int value)
{
	uint16_t data;
	ssize_t n;

	data = value;
	n = pmbus_write(INA233_ADDR, 0xd4, &data, sizeof(data));
	if (n != sizeof(data))
		return;
}

static void udi_cdc_puts(const char *str)
{
	const char *s = str;

	while (*s)
		udi_cdc_multi_putc(0, *s++);
}

#define INA233_READ_VIN	0x88
#define INA233_READ_IIN	0x89

static char *int_to_str(uint32_t value)
{
	static char buf[10];
	char *p = &buf[9];
	uint8_t neg = value < 0;

	if (neg)
		value = -value;

	*p = '\0';

	do {
		*--p = "0123456789"[value % 10];
	} while(value /= 10);

	if (neg)
		*--p = '-';
	return p;
}

static void ina233_status(void)
{
	int value;
	int16_t vin;
	int16_t iin;
	ssize_t n;
	char *str;

	n = pmbus_read(INA233_ADDR, INA233_READ_VIN, &vin, sizeof(vin));
	if (n != sizeof(vin))
		return;
	n = pmbus_read(INA233_ADDR, INA233_READ_IIN, &iin, sizeof(iin));
	if (n != sizeof(iin))
		return;

	// VIN=((1 / 8) * (reg * (pow(10, -2)) * 1000)
	value = vin * 1.25f;
	str = int_to_str(value);
	udi_cdc_puts(str);
	udi_cdc_puts("mV ");

	// IIN=reg * ((2.621/pow(2, 25)) * 1000)
	value = iin * 0.08f;
	str = int_to_str(value);
	udi_cdc_puts(str);
	udi_cdc_puts("mA\r\n");
}

static void qcomlt_debug_port_setup(void)
{
	struct system_pinmux_config pin_conf;

	/* DC and VBUS enable GPIOs */
	system_pinmux_get_config_defaults(&pin_conf);
	pin_conf.mux_position = SYSTEM_PINMUX_GPIO;
	pin_conf.direction = SYSTEM_PINMUX_PIN_DIR_OUTPUT;

	system_pinmux_pin_set_config(PIN_GPIO_DC_EN, &pin_conf);
	port_pin_set_output_level(PIN_GPIO_DC_EN, 0);

	system_pinmux_pin_set_config(PIN_GPIO_VBUS_EN, &pin_conf);
	port_pin_set_output_level(PIN_GPIO_VBUS_EN, 0);
	system_pinmux_pin_set_config(PIN_TXS_EN, &pin_conf);
	port_pin_set_output_level(PIN_TXS_EN, 0);

	/* SERCOM0 UART on PA14, PA15 */
	system_pinmux_get_config_defaults(&pin_conf);
	pin_conf.mux_position = PORT_PMUX_PMUXE_D_Val;
	pin_conf.direction = SYSTEM_PINMUX_PIN_DIR_OUTPUT;

	system_pinmux_pin_set_config(PIN_PA04, &pin_conf);
	system_pinmux_pin_set_config(PIN_PA05, &pin_conf);

	/* SERCOM1 I2C on PA22, PA23 */
	system_pinmux_get_config_defaults(&pin_conf);
	pin_conf.mux_position = PORT_PMUX_PMUXE_C_Val;
	pin_conf.direction = SYSTEM_PINMUX_PIN_DIR_OUTPUT;

	system_pinmux_pin_set_config(PIN_PA22, &pin_conf);
	system_pinmux_pin_set_config(PIN_PA23, &pin_conf);

	/* PWRBT_N on PA05 */
	system_pinmux_get_config_defaults(&pin_conf);
	pin_conf.mux_position = SYSTEM_PINMUX_GPIO;
	pin_conf.direction = SYSTEM_PINMUX_PIN_DIR_OUTPUT;
	port_pin_set_output_level(PIN_PA06, 1);
	system_pinmux_pin_set_config(PIN_PA06, &pin_conf);

	/* RST_BTN_N on PA06 */
	port_pin_set_output_level(PIN_PA03, 1);
	system_pinmux_pin_set_config(PIN_PA03, &pin_conf);
}

static void qcomlt_debug_pin_assert(int pin, bool asserted)
{
	port_pin_set_output_level(pin, !asserted);
}

static uint32_t readl(unsigned int addr)
{
	return *(uint32_t*)addr;
}

static void qcomlt_debug_set_usb_serial(void)
{
	uint8_t *p = usb_serial_number;
	uint32_t word;
	int i;

	word = readl(0x0080a00c);
	for (i = 0; i < 8; i++) {
		*p++ = "0123456789abcdef"[word & 0xf];
		word >>= 4;
	}

	word = readl(0x0080a040);
	for (i = 0; i < 8; i++) {
		*p++ = "0123456789abcdef"[word & 0xf];
		word >>= 4;
	}

	word = readl(0x0080a044);
	for (i = 0; i < 8; i++) {
		*p++ = "0123456789abcdef"[word & 0xf];
		word >>= 4;
	}

	word = readl(0x0080a048);
	for (i = 0; i < 8; i++) {
		*p++ = "0123456789abcdef"[word & 0xf];
		word >>= 4;
	}

	*p = '\0';
}
/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	int ch;

	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();

	system_init();

	qcomlt_debug_port_setup();

	qcomlt_debug_uart_init(115200);
	i2c_init();

	// Calibration for:
	// Rshunt: 0.02
	// Imax: 2.62A
	//
	// CAL=0,00512 * ((2,621 / pow(2, 15)) * 0.02)
	ina233_calibrate(3200);

	qcomlt_debug_set_usb_serial();
	// Start USB stack to authorize VBus monitoring
	udc_start();

	dmac_uart_start();

	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while (true) {
		sleepmgr_enter_sleep();

		if (udi_cdc_multi_is_rx_ready(0)) {
			ch = udi_cdc_multi_getc(0);

			switch (ch) {
			case 'P':
				port_pin_set_output_level(PIN_GPIO_DC_EN, 1);
				port_pin_set_output_level(PIN_TXS_EN, 1);
				break;
			case 'p':
				port_pin_set_output_level(PIN_TXS_EN, 0);
				port_pin_set_output_level(PIN_GPIO_DC_EN, 0);
				break;
			case 'U':
				port_pin_set_output_level(PIN_GPIO_VBUS_EN, 1);
				break;
			case 'u':
				port_pin_set_output_level(PIN_GPIO_VBUS_EN, 0);
				break;
			case 's':
				ina233_status();
				break;
			case 'b':
			case 'B':
				qcomlt_debug_pin_assert(PIN_PA06, ch == 'B');
				break;
			case 'r':
			case 'R':
				qcomlt_debug_pin_assert(PIN_PA03, ch == 'R');
				break;
			case '?':
				udi_cdc_puts("[Pp]ower [Uu]sb [s]tatus pwr[Bb]tn [Rr]stbtn\r\n");
				break;
			}
		}

		if (udi_cdc_multi_is_rx_ready(1)) {
			ch = udi_cdc_multi_getc(1);
			qcomlt_debug_uart_putc(ch);
		}
	}
}

void main_sof_action(void)
{
	dmac_sof_tick();
}

bool main_cdc_enable(uint8_t port)
{
	return true;
}

void main_cdc_disable(uint8_t port)
{
}
