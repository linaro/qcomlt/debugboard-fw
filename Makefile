# Copyright (c) 2023 Linaro Ltd
#
# Copied from DEVICE_EXAMPLE42 generated from Atmel Studio 7
#

LDFLAGS += -Wl,--start-group -lm -Wl,--end-group -mthumb -Wl,-Map="$(TARGET).map" \
	   --specs=nano.specs -Wl,--gc-sections -mcpu=cortex-m0plus \
	   -T"src/ASF/sam0/utils/linker_scripts/samd11/gcc/samd11d14am_flash.ld"

CFLAGS += -x c -mthumb -Os -ffunction-sections -mlong-calls -g3 \
	 -Wall -std=gnu99 -mcpu=cortex-m0plus \
	-DEXTINT_CALLBACK_MODE=true \
	-DUDD_ENABLE \
	-DARM_MATH_CM0PLUS=true \
	-D__SAMD11D14AM__ \
	-DUSB_DEVICE_LPM_SUPPORT \
	-DBOARD=SAMD11_XPLAINED_PRO \
	-Isrc \
	-Isrc/ASF/common/boards \
	-Isrc/ASF/common/services/sleepmgr \
	-Isrc/ASF/common/services/sleepmgr/samd \
	-Isrc/ASF/common/services/usb \
	-Isrc/ASF/common/services/usb/class/cdc \
	-Isrc/ASF/common/services/usb/class/cdc/device \
	-Isrc/ASF/common/services/usb/udc \
	-Isrc/ASF/common/utils \
	-Isrc/ASF/common/utils/interrupt \
	-Isrc/ASF/sam0/boards/ \
	-Isrc/ASF/sam0/drivers/extint \
	-Isrc/ASF/sam0/drivers/port \
	-Isrc/ASF/sam0/drivers/port/quick_start \
	-Isrc/ASF/sam0/drivers/system \
	-Isrc/ASF/sam0/drivers/system/clock \
	-Isrc/ASF/sam0/drivers/system/clock/clock_samd09_d10_d11 \
	-Isrc/ASF/sam0/drivers/system/interrupt \
	-Isrc/ASF/sam0/drivers/system/interrupt/system_interrupt_samd10_d11 \
	-Isrc/ASF/sam0/drivers/system/pinmux \
	-Isrc/ASF/sam0/drivers/system/pinmux/quick_start \
	-Isrc/ASF/sam0/drivers/system/power/power_sam_d_r_h \
	-Isrc/ASF/sam0/drivers/system/reset/reset_sam_d_r_h \
	-Isrc/ASF/sam0/drivers/usb \
	-Isrc/ASF/sam0/drivers/usb/stack_interface \
	-Isrc/ASF/sam0/utils \
	-Isrc/ASF/sam0/utils/cmsis/samd11/include \
	-Isrc/ASF/sam0/utils/cmsis/samd11/include/component \
	-Isrc/ASF/sam0/utils/cmsis/samd11/include/instance \
	-Isrc/ASF/sam0/utils/cmsis/samd11/include/pio \
	-Isrc/ASF/sam0/utils/cmsis/samd11/source \
	-Isrc/ASF/sam0/utils/header_files \
	-Isrc/ASF/sam0/utils/preprocessor \
	-Isrc/ASF/thirdparty/CMSIS/Include \
	-Isrc/config

OBJS=   src/main.o	\
	src/ASF/common/services/sleepmgr/samd/sleepmgr.o	\
	src/ASF/common/services/usb/class/cdc/device/example4/samd11d14a_samd11_xplained_pro/ui.o	\
	src/ASF/common/services/usb/class/cdc/device/udi_cdc.o	\
	src/ASF/common/services/usb/class/cdc/device/udi_cdc_desc.o	\
	src/ASF/common/services/usb/udc/udc.o	\
	src/ASF/common/utils/interrupt/interrupt_sam_nvic.o	\
	src/ASF/sam0/boards/samd11_xplained_pro/board_init.o	\
	src/ASF/sam0/drivers/extint/extint_callback.o	\
	src/ASF/sam0/drivers/extint/extint_sam_d_r_h/extint.o	\
	src/ASF/sam0/drivers/port/port.o	\
	src/ASF/sam0/drivers/system/clock/clock_samd09_d10_d11/clock.o	\
	src/ASF/sam0/drivers/system/clock/clock_samd09_d10_d11/gclk.o	\
	src/ASF/sam0/drivers/system/interrupt/system_interrupt.o	\
	src/ASF/sam0/drivers/system/pinmux/pinmux.o	\
	src/ASF/sam0/drivers/system/system.o	\
	src/ASF/sam0/drivers/usb/stack_interface/usb_device_udd.o	\
	src/ASF/sam0/drivers/usb/stack_interface/usb_dual.o	\
	src/ASF/sam0/drivers/usb/usb_sam_d_r/usb.o	\
	src/ASF/sam0/utils/cmsis/samd11/source/gcc/startup_samd11.o	\
	src/ASF/sam0/utils/cmsis/samd11/source/system_samd11.o	\
	src/ASF/sam0/utils/syscalls/gcc/syscalls.o

DEPS := $(OBJS:%.o=%.d)	

PREFIX ?= arm-none-eabi-
CC = $(PREFIX)gcc
OBJCOPY ?= $(PREFIX)objcopy
OBJDUMP ?= $(PREFIX)objdump
OPENOCD ?= openocd
GDB ?= gdb-multiarch

TARGET = qcomlt_debugboard_fw

all: $(TARGET).bin $(TARGET).hex $(TARGET).eep $(TARGET).lss

%.o: %.c
	@echo CC	$@
	@$(CC) $(CFLAGS) -c -MD -MP -MF "$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -o "$@" "$<"

$(TARGET).elf: $(OBJS)
	@echo LINK	$@
	@$(CC) -o $@ $(LDFLAGS) $(OBJS)

%.bin: %.elf
	@echo OBJCOPY	$@
	@$(OBJCOPY) -O binary $< $@

%.hex: %.elf
	@echo OBJCOPY	$@
	@$(OBJCOPY) -O ihex -R .eeprom -R .fuse -R .lock -R .signature $< $@

%.eep: %.elf
	@echo OBJCOPY	$@
	@$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O binary $< $@

%.lss: %.elf
	@echo OBJDUMP	$@
	@$(OBJDUMP) -h -S $< > $@

flash: $(TARGET).elf
	@echo OPENOCD	openocd-debugboard.cfg
	@$(OPENOCD) -f openocd-debugboard.cfg &
	@echo GDB	$<
	@$(GDB) $< -x flash.gdb

clean:
	@echo RM	*.o
	@-rm $(OBJS)
	@echo RM	*.d
	@-rm $(DEPS)
	@echo RM	$(TARGET).*
	@-rm $(TARGET).*

ifneq ($(strip $(DEPS)),)
-include $(DEPS)
endif
