QcomLT DebugBoard Firmware
==========================

This is the [linaro/qcomlt/debugboard](https://git.codelinaro.org/linaro/qcomlt/debugboard) firmware for the samd11d14am MCU.

The firmware is based on the [Atmel ASF](https://www.microchip.com/en-us/tools-resources/develop/libraries/advanced-software-framework) and [ARM CMSIS](https://developer.arm.com/tools-and-software/embedded/cmsis).

Prerequisites
-------------

The firmware uses the arm-none-eabi GCC toolchain, Make, OpenOCD and GDB to flash the device with the [Atmel ICE](https://www.microchip.com/en-us/development-tool/ATATMEL-ICE).

Under Debian or a derivative, please run:

```
sudo apt install build-essential gcc-arm-none-eabi gdb-multiarch openocd
```

Building
--------

Simply run:

```
make
```

Flashing
--------

Make sure the Atmel ICE is connected to the host machine, and connected to the debug board, then run:

```
make flash
```

Licence
-------

The software is distributed following the [Atmel ASF License](/ASF-License.txt) + the [ARM CMSIS License](/CMSIS-license.txt) for the CMSIS code and macros.
